const electron = require("electron");
const path = require("path");
const env = require("./env");
const menu = require("./menu");

electron.app.on("ready", function() {
  electron.Menu.setApplicationMenu(menu);

  const mainWindow = new electron.BrowserWindow({
    width: 1280,
    height: 720,
    webPreferences: {
      contextIsolation: true,
      preload: path.join(__dirname, "preload.js"),
      nodeIntegration: false,
      nodeIntegrationInWorker: false
    },
    resizable: true,
    show: true
  });

  // mainWindow.setMenuBarVisibility(false);

  mainWindow.loadURL(env.webApp);
});
