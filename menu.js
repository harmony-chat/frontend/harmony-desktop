const electron = require("electron");

module.exports = electron.Menu.buildFromTemplate([
  {
    label: "&File",
    submenu: [
      {
        label: "E&xit",
        click: () => electron.app.quit(),
        accelerator: "CommandOrControl+Q"
      }
    ]
  },
  {
    label: "&Edit",
    submenu: [
      {
        role: "undo",
        accelerator: "CommandOrControl+Z"
      },
      {
        role: "redo",
        accelerator: "Control+Shift+Z"
      },
      {type: "separator"},
      {
        role: "cut",
        accelerator: "CommandOrControl+X"
      },
      {
        role: "copy",
        accelerator: "CommandOrControl+C"
      },
      {
        role: "paste",
        accelerator: "CommandOrControl+V"
      },
      {
        role: "selectAll",
        accelerator: "CommandOrControl+A"
      }
    ]
  },
  {
    label: "&View",
    submenu: [
      {
        label: "&Reload",
        click: () =>
          electron.BrowserWindow.getFocusedWindow().webContents.reloadIgnoringCache(),
        accelerator: "CommandOrControl+R"
      },
      {
        label: "Toggle &Full Screen",
        click: () =>
          electron.BrowserWindow.getFocusedWindow().setFullScreen(
            !electron.BrowserWindow.getFocusedWindow().isFullScreen()
          ),
        accelerator: "CommandOrControl+Shift+F"
      },
      {type: "separator"},
      {
        label: "&Developer",
        submenu: [
          {
            label: "Toggle Developer &Tools",
            click: () =>
              electron.BrowserWindow.getFocusedWindow().toggleDevTools(),
            accelerator:
              process.platform == "darwin" ? "Command+Alt+I" : "Control+Shift+I"
          }
        ]
      }
    ]
  }
]);
