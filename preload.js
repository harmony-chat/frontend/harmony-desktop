const Electron = require("electron");
const env = require("./env");
const appUrl = require("url").parse(env.webApp);
const origin = appUrl.protocol + "//" + appUrl.host;

localStorage.setItem("API_BASE", env.apiBase || "");

window.addEventListener("message", async function(msg) {
  if (origin != msg.origin) return;
  if (msg.data.reply) return console.warn("Got our own message back?");
  console.log("PRELOAD MSG", msg);
  function reply(data) {
    console.log({
      action: msg.data.action,
      body: JSON.stringify(data),
      nonce: msg.data.nonce,
      reply: true
    });
    window.postMessage({
      action: msg.data.action,
      body: JSON.stringify(data),
      nonce: msg.data.nonce,
      reply: true
    });
  }
  if (msg.data.action == "enumerateDisplays") {
    const displaySources = await Electron.desktopCapturer.getSources({
      types: ["window", "screen"]
    });
    reply(
      displaySources.map(source => ({
        id: source.id,
        name: source.name,
        displayId: source.display_id,
        thumbnail: source.thumbnail.toDataURL()
      }))
    );
  }
});
